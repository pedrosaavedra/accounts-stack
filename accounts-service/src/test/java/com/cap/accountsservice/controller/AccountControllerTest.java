package com.cap.accountsservice.controller;

import com.cap.accountsservice.dto.AccountTransactionDTO;
import com.cap.accountsservice.repository.AccountRepository;
import com.cap.accountsservice.repository.UserRepository;
import com.cap.accountsservice.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.math.BigDecimal;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {AccountController.class})
public class AccountControllerTest {

    @MockBean
    private AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Captor
    private ArgumentCaptor<AccountTransactionDTO> accountTransactionCaptor;

    @Test
    public void shouldReturnUnauthorized() throws Exception {
        var customerId = UUID.randomUUID().toString();
        var accountTransaction = AccountTransactionDTO.builder()
                .customerId(customerId)
                .initialCredit(BigDecimal.TEN)
                .build();

        String requestJson = this.objectMapper.writeValueAsString(accountTransaction);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isUnauthorized());

        verify(this.accountService, never()).performAccountTransaction(any());
    }

    @Test
    @WithMockUser
    public void shouldCreateAnAccountWithBalance() throws Exception {
        var customerId = UUID.randomUUID().toString();
        var accountTransaction = AccountTransactionDTO.builder()
                .customerId(customerId)
                .initialCredit(BigDecimal.TEN)
                .build();

        String requestJson = this.objectMapper.writeValueAsString(accountTransaction);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isCreated());

        verify(this.accountService, atLeastOnce()).performAccountTransaction(this.accountTransactionCaptor.capture());

        assertThat(this.accountTransactionCaptor.getValue().getCustomerId(), is(equalTo(accountTransaction.getCustomerId())));
        assertThat(this.accountTransactionCaptor.getValue().getInitialCredit(), is(equalTo(accountTransaction.getInitialCredit())));
    }

    @Test
    @WithMockUser
    public void shouldReturn400ErrorWhenCustomerIdIsNull() throws Exception {
        var accountTransaction = AccountTransactionDTO.builder()
                .initialCredit(BigDecimal.TEN)
                .build();

        var result = this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .content(this.objectMapper.writeValueAsString(accountTransaction)))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andDo(print())
                .andExpect(content().string(containsStringIgnoringCase("customer id can not be null or empty")))
                .andReturn();

        assertThat(result.getResolvedException(), instanceOf(MethodArgumentNotValidException.class));

        verify(this.accountService, never()).performAccountTransaction(any());
    }

    @Test
    @WithMockUser
    public void shouldReturn400ErrorWhenInitialCreditIsNull() throws Exception {
        var customerId = UUID.randomUUID().toString();
        var accountTransaction = AccountTransactionDTO.builder()
                .customerId(customerId)
                .build();

        var result = this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .content(this.objectMapper.writeValueAsString(accountTransaction)))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andDo(print())
                .andExpect(content().string(containsStringIgnoringCase("Initial credit can not be null or empty")))
                .andReturn();

        assertThat(result.getResolvedException(), instanceOf(MethodArgumentNotValidException.class));

        verify(this.accountService, never()).performAccountTransaction(any());
    }

    @Test
    @WithMockUser
    public void shouldReturn400ErrorWhenInitialCreditIsNotValid() throws Exception {
        var body = "{\"customerId\":\"b6e43bed-32e8-4a31-8af9-3ee5c8272b91\",\"initialCredit\":\"abc\"}";
        var result = this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf())
                .content(body))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content().string(containsStringIgnoringCase("Initial credit must be a valid currency")))
                .andReturn();

        assertThat(result.getResolvedException(), instanceOf(HttpMessageNotReadableException.class));

        verify(this.accountService, never()).performAccountTransaction(any());
    }
}
