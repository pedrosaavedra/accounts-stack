package com.cap.accountsservice.controller;

import com.cap.accountsservice.dto.UserDTO;
import com.cap.accountsservice.repository.UserRepository;
import com.cap.accountsservice.service.UserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {UserController.class})
public class UserControllerTest {

    @MockBean
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnUnauthorized() throws Exception {
        this.mockMvc.perform(get("/users"))
                .andExpect(status().isUnauthorized());

        verify(this.userService, never()).findAll();
    }

    @Test
    @WithMockUser
    public void shouldReturnAllUsers() throws Exception {

        when(this.userService.findAll()).thenReturn(List.of(createUser("user1"), createUser("user2")));

        this.mockMvc.perform(get("/users"))
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].id", equalTo("123")))
                .andExpect(jsonPath("$[1].id", equalTo("123")))
                .andExpect(jsonPath("$[0].name", equalTo("user1")))
                .andExpect(jsonPath("$[1].name", equalTo("user2")))
                .andExpect(jsonPath("$[0].balance", equalTo(10)))
                .andExpect(jsonPath("$[1].balance", equalTo(10)))
                .andExpect(jsonPath("$[0].email", equalTo("user1@test.com")))
                .andExpect(jsonPath("$[1].email", equalTo("user2@test.com")))
                .andExpect(status().isOk());

        verify(this.userService, times(1)).findAll();
    }

    private UserDTO createUser(String name) {
        return UserDTO.builder()
                .id("123")
                .balance(BigDecimal.TEN)
                .name(name)
                .surname(name)
                .email(name + "@test.com")
                .build();
    }
}
