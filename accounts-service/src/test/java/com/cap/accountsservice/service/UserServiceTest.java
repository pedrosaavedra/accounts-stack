package com.cap.accountsservice.service;

import com.cap.accountsservice.client.TransactionsClient;
import com.cap.accountsservice.dto.TransactionDTO;
import com.cap.accountsservice.entity.Account;
import com.cap.accountsservice.entity.User;
import com.cap.accountsservice.repository.UserRepository;
import com.cap.accountsservice.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TransactionsClient transactionsClient;

    @BeforeEach
    public void beforeClass(){
        this.userService = new UserServiceImpl(this.userRepository, this.transactionsClient);
    }

    @Test
    public void shouldReturnAllUsersWithoutAccountAndTransactions(){
        var customerId1 = UUID.randomUUID().toString();
        var customerId2 = UUID.randomUUID().toString();

        when(this.userRepository.findAll()).thenReturn(List.of(getUser(customerId1), getUser(customerId2)));

        var allUsers = this.userService.findAll();

        assertThat(allUsers.size(), is(equalTo(2)));
        assertThat(allUsers.get(0).getName(), is(equalTo("user"+customerId1)));
        assertThat(allUsers.get(1).getName(), is(equalTo("user"+customerId2)));
        assertThat(allUsers.get(0).getAccountType(), is(nullValue()));
        assertThat(allUsers.get(1).getAccountType(), is(nullValue()));
        assertThat(allUsers.get(0).getTransactions(),is(nullValue()));
        assertThat(allUsers.get(1).getTransactions(),is(nullValue()));

        verify(this.transactionsClient, never()).performTransaction(any());
    }

    @Test
    public void shouldReturnAllUsersWithAccountAndTransactions(){
        var customerId1 = UUID.randomUUID().toString();
        var customerId2 = UUID.randomUUID().toString();

        var userEntity1 = getUser(customerId1);
        var acc1 = new Account();
        acc1.setId("123");
        acc1.setAccountType(Account.AccountType.BASIC);
        userEntity1.setAccount(acc1);

        var userEntity2 = getUser(customerId2);
        var acc2 = new Account();
        acc2.setId("321");
        acc2.setAccountType(Account.AccountType.BASIC);
        userEntity2.setAccount(acc2);

        when(this.userRepository.findAll()).thenReturn(List.of(userEntity1, userEntity2));

        var transaction1 = getTransaction(acc1.getId());
        var transaction2 = getTransaction(acc2.getId());
        when(this.transactionsClient.findTransactionsByAccountId(acc1.getId())).thenReturn(List.of(transaction1));
        when(this.transactionsClient.findTransactionsByAccountId(acc2.getId())).thenReturn(List.of(transaction2));

        var allUsers = this.userService.findAll();

        assertThat(allUsers.size(), is(equalTo(2)));
        assertThat(allUsers.get(0).getName(), is(equalTo("user"+customerId1)));
        assertThat(allUsers.get(1).getName(), is(equalTo("user"+customerId2)));
        assertThat(allUsers.get(0).getAccountType(), is(equalTo(Account.AccountType.BASIC.name())));
        assertThat(allUsers.get(1).getAccountType(), is(equalTo(Account.AccountType.BASIC.name())));
        assertThat(allUsers.get(0).getTransactions(),is(equalTo(List.of(transaction1))));
        assertThat(allUsers.get(1).getTransactions(),is(equalTo(List.of(transaction2))));

        verify(this.transactionsClient, times(1)).findTransactionsByAccountId("123");
        verify(this.transactionsClient, times(1)).findTransactionsByAccountId("321");
    }

    private static User getUser(String customerId) {
        var userEntity = new User();
        userEntity.setId(customerId);
        userEntity.setName("user"+customerId);
        userEntity.setSurname("userSurname");
        userEntity.setEmail("user@user.com");

        return userEntity;
    }

    private static TransactionDTO getTransaction(String accountId) {
        var transaction = new TransactionDTO();
        transaction.setAccountId(accountId);
        transaction.setOperationType(TransactionDTO.operationType.ACCOUNT_CREATION.name());
        return transaction;
    }
}
