package com.cap.accountsservice.service;

import com.cap.accountsservice.client.TransactionsClient;
import com.cap.accountsservice.dto.AccountTransactionDTO;
import com.cap.accountsservice.dto.TransactionDTO;
import com.cap.accountsservice.entity.Account;
import com.cap.accountsservice.entity.User;
import com.cap.accountsservice.exception.CustomerNotFoundException;
import com.cap.accountsservice.repository.UserRepository;
import com.cap.accountsservice.service.impl.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class AccountServiceTest {
    @InjectMocks
    private AccountServiceImpl accountService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TransactionsClient transactionsClient;

    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    @Captor
    ArgumentCaptor<TransactionDTO> transactionArgumentCaptor;

    @BeforeEach
    public void beforeEachTest(){
        this.accountService = new AccountServiceImpl(this.userRepository, this.transactionsClient);
    }

    @Test
    public void shouldThrowAnUserNotFoundException() {
        when(this.userRepository.findById("123")).thenReturn(Optional.empty());

        var transactionDto = AccountTransactionDTO.builder()
                .customerId("123")
                .initialCredit(BigDecimal.TEN)
                .build();

        try {
            this.accountService.performAccountTransaction(transactionDto);
        } catch (CustomerNotFoundException ex) {
            assertTrue(true);
        }
    }

    @Test
    public void shouldPerformOperationCreationAccount(){
        var customerId = UUID.randomUUID().toString();
        var userEntity = new User();
        userEntity.setId(customerId);
        userEntity.setName("user");
        userEntity.setSurname("userSurname");
        userEntity.setEmail("user@user.com");

        when(this.userRepository.findById(customerId)).thenReturn(Optional.of(userEntity));

        var transactionDto = AccountTransactionDTO.builder()
                .customerId(customerId)
                .initialCredit(BigDecimal.TEN)
                .build();

        this.accountService.performAccountTransaction(transactionDto);

        verify(this.userRepository, atLeastOnce()).save(this.userArgumentCaptor.capture());
        verify(this.transactionsClient, atLeastOnce()).performTransaction(this.transactionArgumentCaptor.capture());

        assertThat(this.userArgumentCaptor.getValue().getAccount().getUser().getId(), is(equalTo(customerId)));
        assertThat(this.transactionArgumentCaptor.getValue().getOperationType(), is(equalTo(TransactionDTO.operationType.ACCOUNT_CREATION.name())));
    }

    @Test
    public void shouldPerformOperationCreditValue(){
        var customerId = UUID.randomUUID().toString();
        var userEntity = new User();
        userEntity.setId(customerId);
        userEntity.setName("user");
        userEntity.setSurname("userSurname");
        userEntity.setEmail("user@user.com");

        var account = new Account();
        account.setBalance(BigDecimal.TEN);
        account.setUser(userEntity);
        userEntity.setAccount(account);

        when(this.userRepository.findById(customerId)).thenReturn(Optional.of(userEntity));

        var transactionDto = AccountTransactionDTO.builder()
                .customerId(customerId)
                .initialCredit(BigDecimal.TEN)
                .build();

        this.accountService.performAccountTransaction(transactionDto);

        verify(this.userRepository, atLeastOnce()).save(this.userArgumentCaptor.capture());
        verify(this.transactionsClient, atLeastOnce()).performTransaction(this.transactionArgumentCaptor.capture());

        assertThat(this.userArgumentCaptor.getValue().getAccount().getUser().getId(), is(equalTo(customerId)));
        assertThat(this.userArgumentCaptor.getValue().getAccount().getBalance(), is(equalTo(BigDecimal.valueOf(20))));
        assertThat(this.transactionArgumentCaptor.getValue().getOperationType(), is(equalTo(TransactionDTO.operationType.CREDIT.name())));
    }

    @Test
    public void shouldPerformOperationDebitValue(){
        var customerId = UUID.randomUUID().toString();
        var userEntity = new User();
        userEntity.setId(customerId);
        userEntity.setName("user");
        userEntity.setSurname("userSurname");
        userEntity.setEmail("user@user.com");

        var account = new Account();
        account.setBalance(BigDecimal.valueOf(-10));
        account.setUser(userEntity);
        userEntity.setAccount(account);

        when(this.userRepository.findById(customerId)).thenReturn(Optional.of(userEntity));

        var transactionDto = AccountTransactionDTO.builder()
                .customerId(customerId)
                .initialCredit(BigDecimal.TEN)
                .build();

        this.accountService.performAccountTransaction(transactionDto);

        verify(this.userRepository, atLeastOnce()).save(this.userArgumentCaptor.capture());
        verify(this.transactionsClient, atLeastOnce()).performTransaction(this.transactionArgumentCaptor.capture());

        assertThat(this.userArgumentCaptor.getValue().getAccount().getUser().getId(), is(equalTo(customerId)));
        assertThat(this.userArgumentCaptor.getValue().getAccount().getBalance(), is(equalTo(BigDecimal.valueOf(0))));
        assertThat(this.transactionArgumentCaptor.getValue().getOperationType(), is(equalTo(TransactionDTO.operationType.CREDIT.name())));
    }
}
