package com.cap.accountsservice.service;

import com.cap.accountsservice.dto.AccountTransactionDTO;

public interface AccountService {
    void performAccountTransaction(AccountTransactionDTO accountTransaction);
}
