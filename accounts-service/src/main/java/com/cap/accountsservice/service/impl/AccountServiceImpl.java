package com.cap.accountsservice.service.impl;

import com.cap.accountsservice.client.TransactionsClient;
import com.cap.accountsservice.dto.AccountTransactionDTO;
import com.cap.accountsservice.dto.TransactionDTO;
import com.cap.accountsservice.entity.Account;
import com.cap.accountsservice.exception.CustomerNotFoundException;
import com.cap.accountsservice.repository.UserRepository;
import com.cap.accountsservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class AccountServiceImpl implements AccountService {

    private UserRepository userRepository;
    private TransactionsClient transactionsClient;

    @Autowired
    public AccountServiceImpl(UserRepository userRepository, TransactionsClient transactionsClient) {
        this.userRepository = userRepository;
        this.transactionsClient = transactionsClient;
    }

    @Override
    @Transactional
    public void performAccountTransaction(AccountTransactionDTO accountTransaction) {
        var user = this.userRepository.findById(accountTransaction.getCustomerId())
                .orElseThrow(() -> new CustomerNotFoundException(accountTransaction.getCustomerId()));

        var transactionDto = new TransactionDTO();
        transactionDto.setValue(accountTransaction.getInitialCredit());

        if (!Objects.nonNull(user.getAccount())) {
            var account = new Account();
            account.setUser(user);
            account.setAccountType(Account.AccountType.BASIC);
            account.setBalance(accountTransaction.getInitialCredit());
            user.setAccount(account);
            this.userRepository.save(user);
            transactionDto.setOperationType(TransactionDTO.operationType.ACCOUNT_CREATION.name());
        } else {
            var currentBalance = user.getAccount().getBalance();
            if (accountTransaction.getInitialCredit().intValue() >= 0) {
                transactionDto.setOperationType(TransactionDTO.operationType.CREDIT.name());
            } else {
                transactionDto.setOperationType(TransactionDTO.operationType.DEBIT.name());
            }
            user.getAccount().setBalance(currentBalance.add(accountTransaction.getInitialCredit()));
            this.userRepository.save(user);
        }

        transactionDto.setAccountId(user.getAccount().getId());
        this.transactionsClient.performTransaction(transactionDto);
    }
}
