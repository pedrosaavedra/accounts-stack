package com.cap.accountsservice.service.impl;

import com.cap.accountsservice.client.TransactionsClient;
import com.cap.accountsservice.dto.UserDTO;
import com.cap.accountsservice.entity.User;
import com.cap.accountsservice.repository.UserRepository;
import com.cap.accountsservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository customerRepository;
    private static TransactionsClient transactionsClient;

    @Autowired
    public UserServiceImpl(UserRepository customerRepository, TransactionsClient transactionsClient) {
        this.customerRepository = customerRepository;
        this.transactionsClient = transactionsClient;
    }

    @Override
    public List<UserDTO> findAll() {
        return this.customerRepository.findAll().stream()
                .map(UserServiceImpl::toUserDTO)
                .collect(Collectors.toList());
    }

    private static UserDTO toUserDTO(User customer) {
        var userDto = UserDTO.builder()
                .id(customer.getId())
                .name(customer.getName())
                .email(customer.getEmail())
                .surname(customer.getSurname());

        Optional.ofNullable(customer.getAccount()).ifPresent(acc -> {
            userDto.balance(acc.getBalance());
            userDto.accountType(acc.getAccountType().name());
            userDto.transactions(transactionsClient.findTransactionsByAccountId(acc.getId()));
        });

        return userDto.build();
    }
}
