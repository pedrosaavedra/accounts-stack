package com.cap.accountsservice.service;

import com.cap.accountsservice.dto.UserDTO;

import java.util.List;

public interface UserService {
    List<UserDTO> findAll();
}
