package com.cap.accountsservice.client;

import com.cap.accountsservice.dto.TransactionDTO;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

@Component
public class TransactionsClient {

    private RestTemplate restTemplate;
    private String transactionsServiceUrl;
    private String transactionServiceUsername;
    private String transactionServicePassword;

    @Autowired
    public TransactionsClient(RestTemplate restTemplate,
                              @Value("${client.transaction-service.username}") String transactionServiceUsername,
                              @Value("${client.transaction-service.password}") String transactionServicePassword,
                              @Value("${client.transaction-service.url}") String transactionsServiceUrl) {
        this.restTemplate = restTemplate;
        this.transactionsServiceUrl = transactionsServiceUrl;
        this.transactionServiceUsername = transactionServiceUsername;
        this.transactionServicePassword = transactionServicePassword;
    }

    public void performTransaction(TransactionDTO transactionDto) {
        var requestBody = new HttpEntity<TransactionDTO>(transactionDto, this.getHeader());
        try {
            restTemplate.postForObject(transactionsServiceUrl + "/transactions", requestBody, TransactionDTO.class);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<TransactionDTO> findTransactionsByAccountId(String accountId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(transactionsServiceUrl + "/transactions")
                .queryParam("accountId", accountId);

        HttpEntity<?> entity = new HttpEntity<>(this.getHeader());
        HttpEntity<List<TransactionDTO>> response = null;
        try {
            response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<List<TransactionDTO>>() {
                    });
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return response.getBody();
    }

    private HttpHeaders getHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        String auth = transactionServiceUsername + ":" + transactionServicePassword;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        headers.set("Authorization", authHeader);

        return headers;
    }
}
