package com.cap.accountsservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder
public class UserDTO {
    private String id;
    private String name;
    private String email;
    private String surname;

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String accountType;

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private BigDecimal balance;

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private List<TransactionDTO> transactions;
}
