package com.cap.accountsservice.dto;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.math.BigDecimal;
import java.util.Optional;

public class CustomInitialCreditDeserializer extends JsonDeserializer<BigDecimal> {

    @Override
    public BigDecimal deserialize(JsonParser jsonParser, DeserializationContext context) {
        try {
            var value = jsonParser.readValuesAs(String.class).next();
            var maybeAmount = Optional.ofNullable(value)
                    .map(v -> Long.valueOf(v.contains(".") ? v.replaceAll("\\.", "") : v));

            return maybeAmount.isPresent() ? BigDecimal.valueOf(maybeAmount.get()) : null;
        } catch (Exception e) {
            throw new RuntimeException("Initial credit must be a valid currency without comma.");
        }
    }
}
