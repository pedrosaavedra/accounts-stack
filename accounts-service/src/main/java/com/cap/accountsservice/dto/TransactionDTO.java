package com.cap.accountsservice.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class TransactionDTO {
    private String accountId;
    private String operationType;
    private BigDecimal value;
    private LocalDateTime createdOn;

    public enum operationType {
        ACCOUNT_CREATION,CREDIT,DEBIT;
    }
}
