package com.cap.accountsservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Builder
public class AccountTransactionDTO {

    @NotBlank(message = "customer id can not be null or empty")
    private String customerId;

    @NotNull(message = "Initial credit can not be null or empty")
    @JsonDeserialize(using = CustomInitialCreditDeserializer.class)
    private BigDecimal initialCredit;

    @JsonCreator
    public static AccountTransactionDTO create(
            @JsonProperty("customerId") String customerId,
            @JsonProperty("initialCredit") BigDecimal initialCredit) {

        return AccountTransactionDTO.builder()
                .customerId(customerId)
                .initialCredit(initialCredit)
                .build();
    }
}
