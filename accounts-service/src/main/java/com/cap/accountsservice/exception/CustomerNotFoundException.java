package com.cap.accountsservice.exception;

public class CustomerNotFoundException extends RuntimeException {

    public CustomerNotFoundException(String id) {
        super(String.format("Customer with Id %s not found", id));
    }
}
