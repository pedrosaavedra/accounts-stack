drop table if exists account CASCADE;
drop table if exists user CASCADE;
create table account
(
    id           varchar(255) not null,
    account_type varchar(255),
    balance      decimal(19, 2),
    user_id      varchar(255),
    primary key (id)
);

create table user
(
    id      varchar(255) not null,
    age     integer,
    email   varchar(255) not null,
    name    varchar(255) not null,
    surname varchar(255) not null,
    primary key (id)
);

alter table account add constraint FK7m8ru44m93ukyb61dfxw0apf6 foreign key (user_id) references user;