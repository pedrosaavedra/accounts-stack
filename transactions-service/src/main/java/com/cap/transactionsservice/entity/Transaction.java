package com.cap.transactionsservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "account_transaction")
public class Transaction {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "account_id", nullable = false)
    private String accountId;

    @Column(name = "operation_type", nullable = false)
    private String operationType;

    @Column(name = "value", nullable = false)
    private BigDecimal value;

    @Column(name = "created_on")
    private LocalDateTime createdOn = LocalDateTime.now();
}
