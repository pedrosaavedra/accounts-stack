package com.cap.transactionsservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Builder
public class TransactionDTO {

    @NotBlank(message = "Account id can not be null or empty")
    private String accountId;
    @NotBlank(message = "Operation type can not be null or empty")
    private String operationType;

    @NotNull(message = "Value can not be null or empty")
    @JsonDeserialize(using = ValueCreditDebitDeserializer.class)
    private BigDecimal value;
    private LocalDateTime createdOn;

    @JsonCreator
    public static TransactionDTO create(
            @JsonProperty("accountId") String accountId,
            @JsonProperty("operationType") String operationType,
            @JsonProperty("value") BigDecimal value
    ) {
        return TransactionDTO.builder()
                .accountId(accountId)
                .operationType(operationType)
                .value(value)
                .build();
    }
}
