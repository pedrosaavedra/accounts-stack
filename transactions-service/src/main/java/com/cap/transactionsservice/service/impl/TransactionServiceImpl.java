package com.cap.transactionsservice.service.impl;

import com.cap.transactionsservice.dto.TransactionDTO;
import com.cap.transactionsservice.entity.Transaction;
import com.cap.transactionsservice.repository.TransactionRepository;
import com.cap.transactionsservice.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public void performTransaction(TransactionDTO transaction) {
        this.transactionRepository.save(toTransaction(transaction));
    }

    @Override
    public List<TransactionDTO> findAllByAccountId(String accountId) {
        return this.transactionRepository.findAllByAccountId(accountId)
                .stream()
                .map(TransactionServiceImpl::toTransactionDTO)
                .collect(Collectors.toList());
    }

    private static TransactionDTO toTransactionDTO(Transaction transaction) {
        return TransactionDTO.builder()
                .value(transaction.getValue())
                .accountId(transaction.getAccountId())
                .createdOn(transaction.getCreatedOn())
                .operationType(transaction.getOperationType())
                .build();
    }

    private static Transaction toTransaction(TransactionDTO transactionDTO) {
        var transaction = new Transaction();
        transaction.setAccountId(transactionDTO.getAccountId());
        transaction.setValue(transactionDTO.getValue());
        transaction.setOperationType(transactionDTO.getOperationType());

        return transaction;
    }
}
