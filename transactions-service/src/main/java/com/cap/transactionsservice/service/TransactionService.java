package com.cap.transactionsservice.service;

import com.cap.transactionsservice.dto.TransactionDTO;

import java.util.List;

public interface TransactionService {
    void performTransaction(TransactionDTO transaction);

    List<TransactionDTO> findAllByAccountId(String accountId);
}
