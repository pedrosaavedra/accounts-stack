package com.cap.transactionsservice.controller;

import com.cap.transactionsservice.dto.TransactionDTO;
import com.cap.transactionsservice.service.TransactionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SecurityRequirement(name = "transapi")
@Tag(name = "Transactions", description = "The transactions API")
@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Operation(summary = "Create a transaction of an account")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Account created"),
            @ApiResponse(responseCode = "400", description = "Invalid request body", content = @Content)
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity performTransaction(@RequestBody @Validated TransactionDTO transaction) {
        this.transactionService.performTransaction(transaction);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Find transactions of an account")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "All transactions of an account",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = TransactionDTO.class))
                    })
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TransactionDTO>> findAllByAccountId(@RequestParam("accountId") String accountId) {
        return ResponseEntity.ok(this.transactionService.findAllByAccountId(accountId));
    }
}
