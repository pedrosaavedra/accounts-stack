package com.cap.transactionsservice.service;

import com.cap.transactionsservice.dto.TransactionDTO;
import com.cap.transactionsservice.entity.Transaction;
import com.cap.transactionsservice.repository.TransactionRepository;
import com.cap.transactionsservice.service.impl.TransactionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class TransactionServiceTest {

    private TransactionService transactionService;

    @Mock
    private TransactionRepository transactionRepository;

    @Captor
    private ArgumentCaptor<Transaction> transactionArgumentCaptor;

    @BeforeEach
    public void beforeEachTest() {
        this.transactionService = new TransactionServiceImpl(this.transactionRepository);
    }

    @Test
    public void shouldPerformATransaction() {
        var accountId = UUID.randomUUID().toString();
        var transaction = getTransaction(accountId);

        when(this.transactionRepository.save(transaction)).thenReturn(transaction);

        this.transactionService.performTransaction(getTransactionDTO(accountId));

        verify(this.transactionRepository, times(1)).save(this.transactionArgumentCaptor.capture());

        assertThat(this.transactionArgumentCaptor.getValue().getAccountId(), is(equalTo(accountId)));
        assertThat(this.transactionArgumentCaptor.getValue().getOperationType(), is(equalTo("CREDIT")));
        assertThat(this.transactionArgumentCaptor.getValue().getValue(), is(equalTo(BigDecimal.TEN)));
    }

    @Test
    public void shouldFindAllTransactionsByAccountId() {
        var accountId = UUID.randomUUID().toString();
        var transaction = getTransaction(accountId);

        when(this.transactionRepository.findAllByAccountId(accountId)).thenReturn(List.of(transaction));

        var transactions = this.transactionService.findAllByAccountId(accountId);

        assertThat(transactions.size(), is(equalTo(1)));
        assertThat(transactions.get(0).getAccountId(), is(equalTo(accountId)));
        assertThat(transactions.get(0).getValue(), is(equalTo(BigDecimal.TEN)));
        assertThat(transactions.get(0).getOperationType(), is(equalTo("CREDIT")));

        verify(this.transactionRepository, times(1)).findAllByAccountId(accountId);
    }

    private static Transaction getTransaction(String accountId) {
        var transaction = new Transaction();
        transaction.setId(UUID.randomUUID().toString());
        transaction.setAccountId(accountId);
        transaction.setOperationType("CREDIT");
        transaction.setValue(BigDecimal.TEN);
        transaction.setCreatedOn(LocalDateTime.now());
        return transaction;
    }

    private static TransactionDTO getTransactionDTO(String accountId){
        return TransactionDTO.builder()
                .accountId(accountId)
                .operationType("CREDIT")
                .value(BigDecimal.TEN)
                .build();
    }
}
