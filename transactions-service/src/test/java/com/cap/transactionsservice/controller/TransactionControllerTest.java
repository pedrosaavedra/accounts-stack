package com.cap.transactionsservice.controller;

import com.cap.transactionsservice.dto.TransactionDTO;
import com.cap.transactionsservice.service.TransactionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {TransactionController.class})
public class TransactionControllerTest {

    @MockBean
    private TransactionService transactionService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Captor
    private ArgumentCaptor<TransactionDTO> transactionDTOArgumentCaptor;

    @Test
    public void shouldReturnUnauthorized() throws Exception {
        var accountTransaction = createTransaction();
        String requestJson = this.objectMapper.writeValueAsString(accountTransaction);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isUnauthorized());

        verify(this.transactionService, never()).performTransaction(any());
    }

    @Test
    @WithMockUser
    public void shouldCreateATransaction() throws Exception {
        var accountTransaction = createTransaction();
        String requestJson = this.objectMapper.writeValueAsString(accountTransaction);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isCreated());

        verify(this.transactionService, atLeastOnce()).performTransaction(this.transactionDTOArgumentCaptor.capture());

        assertThat(this.transactionDTOArgumentCaptor.getValue().getValue(), is(equalTo(accountTransaction.getValue())));
        assertThat(this.transactionDTOArgumentCaptor.getValue().getAccountId(), is(equalTo(accountTransaction.getAccountId())));
        assertThat(this.transactionDTOArgumentCaptor.getValue().getCreatedOn(), is(equalTo(accountTransaction.getCreatedOn())));
        assertThat(this.transactionDTOArgumentCaptor.getValue().getOperationType(), is(equalTo(accountTransaction.getOperationType())));
    }

    @Test
    @WithMockUser
    public void shouldReturn400ErrorWhenRequestBodyIsNotValid() throws Exception {
        var accountTransaction = TransactionDTO.builder()
                .createdOn(LocalDateTime.now())
                .build();
        String requestJson = this.objectMapper.writeValueAsString(accountTransaction);

        var result = this.mockMvc.perform(MockMvcRequestBuilders.post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andDo(print())
                .andExpect(content().string(containsStringIgnoringCase("Account id can not be null or empty")))
                .andExpect(content().string(containsStringIgnoringCase("Operation type can not be null or empty")))
                .andExpect(content().string(containsStringIgnoringCase("Value can not be null or empty")))
                .andReturn();

        assertThat(result.getResolvedException(), instanceOf(MethodArgumentNotValidException.class));

        verify(this.transactionService, never()).performTransaction(any());
    }

    @Test
    @WithMockUser
    public void shouldReturn400ErrorWhenValueIsNotAValidNumberCurrency() throws Exception {
        var requestJson = "{\"accountId\":\"95498d45-a5d6-4c2f-89dd-7db39dd595dc\",\"operationType\":\"CREDIT\",\"value\": \"abc\"}";

        var result = this.mockMvc.perform(MockMvcRequestBuilders.post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andDo(print())
                .andExpect(content().string(containsStringIgnoringCase("Value must be a valid currency without comma")))
                .andReturn();

        requestJson = "{\"accountId\":\"95498d45-a5d6-4c2f-89dd-7db39dd595dc\",\"operationType\":\"CREDIT\",\"value\": \"1,1\"}";

        result = this.mockMvc.perform(MockMvcRequestBuilders.post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andDo(print())
                .andExpect(content().string(containsStringIgnoringCase("Value must be a valid currency without comma")))
                .andReturn();

        assertThat(result.getResolvedException(), instanceOf(HttpMessageNotReadableException.class));

        verify(this.transactionService, never()).performTransaction(any());
    }

    private static TransactionDTO createTransaction(){
        return TransactionDTO.builder()
                .accountId(UUID.randomUUID().toString())
                .createdOn(LocalDateTime.now())
                .value(BigDecimal.TEN)
                .operationType("CREDIT")
                .build();
    }
}
