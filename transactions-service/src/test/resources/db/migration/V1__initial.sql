drop table if exists account_transaction CASCADE;

create table account_transaction
(
    id             varchar(255)   not null,
    account_id     varchar(255)   not null,
    created_on     timestamp,
    operation_type varchar(255)   not null,
    value          decimal(19, 2) not null,
    primary key (id)
);