# Transactions Service

- Transactions service - Spring Boot - H2 DB - Swagger Doc 

# Running the service

- Make sure that the port 8082 is available on your machine
- The service has basic authentication with a fake user, if you try to use your browser Spring security will show a pop up for authentication
  - Username: admin
  - Password: admin

- Enter on transactions-service folder and run the command:
  - mvn spring-boot:run -Dspring-boot.run.arguments="--spring.profiles.active=local"


## H2 Database
- You can access Transactions Service database on URL: http://localhost:8082/h2
  - Driver Class: org.h2.Driver
  - JDBC URL: jdbc:h2:mem:transactions
  - User Name: sa
  - Password: sa
  
## Swagger Documentation
- You can access Swagger on URL: http://localhost:8082/swagger-ui.html  
