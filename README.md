# Accounts Stack

This stack contains:
- Account frontend - VueJS - Bootstrap
- Accounts service - Spring Boot - H2 DB - Swagger Doc 
- Transactions service - Spring Boot - H2 DB - Swagger Doc

# Running the stack

- Make sure that the ports ( 8080 / 8081 / 8082 ) are available on your machine
- The services have basic authentication with a fake user, if you try to use your browser Spring security will show a pop up for authentication
  - Username: admin
  - Password: admin

## There are two ways to run this stack:

### With docker compose
- On the project root folder runs:
  - docker compose up --build

### With Maven and Npm   
- With maven command:
  - Enter on transactions-service folder and run the command:
    - mvn spring-boot:run -Dspring-boot.run.arguments="--spring.profiles.active=local"
  - Enter on accounts-service folder and run the command:
    - mvn spring-boot:run -Dspring-boot.run.arguments="--spring.profiles.active=local"
  - Enter on accounts-front folder and run the command:
    - npm run serve

# Using the stack

## Frontend
- You can access the frontend on URL:  http://localhost:8080

## Accounts service
- You can access Accounts Service database on URL: http://localhost:8081/h2
  - Driver Class: org.h2.Driver
  - JDBC URL: jdbc:h2:mem:accounts
  - User Name: sa
  - Password: sa
- You can access Swagger on URL: http://localhost:8081/swagger-ui.html  

## Transactions service
- You can access Transaction Service database on URL: http://localhost:8082/h2
  - Driver Class: org.h2.Driver
  - JDBC URL: jdbc:h2:mem:transactions
  - User Name: sa
  - Password: sa
- You can access Swagger on URL: http://localhost:8082/swagger-ui.html