import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);

const username = 'admin'
const password = 'admin'
const HEADERS = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Expose-Headers': '*'
};


export default {
    performAccountTransaction(payload) {
        const url = 'http://127.0.0.1:8081/accounts';
        return Vue.axios.post(url, payload, { headers: HEADERS, auth: {
            username: username,
            password: password
          }});
    },
    findAllUsers() {
        const url = 'http://127.0.0.1:8081/users';
        return Vue.axios.get(url, { headers: HEADERS, auth: {
            username: username,
            password: password
          } });
    }
};
