import Vue from 'vue'
import App from './App.vue'
import router from './router'
import money from 'v-money'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

Vue.use(money, {precision: 4})

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false

new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app')
